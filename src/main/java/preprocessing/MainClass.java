package preprocessing;

import java.io.IOException;

public class MainClass {
	/**
	 * Input parameters PROFILES:
	 * 0. input files format
	 * 1. input profiles files (separated by commas)/CSV files
	 * 2. output numeric file path  
	 * 3. output match file path 
	 * 4. input ground truth file path 
	 * 5. input stopwords file path
	 * 6. output lexicon file path
	 * 7. output records file path
	 * 8. n-grams parameter {3,4,5,..}
	 * 9. pruning threshold parameter
	 * 10. dataset type
	 * 11. [limit for data-set size]
	 * 12. [prefix length]
	 * 13. [maximal dense attribute set size] (for huge schema)
	 * @throws IOException
	 */
	public static void main(String[] args) throws IOException{
		String inputFormat = args[0];
		String[] input = new String[args.length-1];
		System.arraycopy(args, 1, input, 0, args.length-1);
		if (inputFormat.equalsIgnoreCase(InputFormat.CSV)){
			csvFile.main(input);
		}
		else if (inputFormat.equalsIgnoreCase(InputFormat.PROFILES)){
			ProfileReader.main(input);
		}
		else if (inputFormat.equalsIgnoreCase(InputFormat.DBPEDIA)){
			DBPediaParser.main(input);
		}
	}
}
