package preprocessing;

public final class InputFormat {
	public static final String CSV = "CSV";
	public static final String PROFILES = "PROFILES";
	public static final String DBPEDIA = "DBPEDIA";
}
