package preprocessing;

public final class ProfileType {
	public static final String DBPEDIA="DBPEDIA";
	public static final String MOVIES="MOVIES";
	public static final String CDDB="CDDB";
    public static final String CORA="CORA";
    public static final String CENSUS="CENSUS";
    public static final String RESTAURANTS="RESTAURANTS";
    
    private ProfileType(){}
}
