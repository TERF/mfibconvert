package preprocessing;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import DataStructures.Attribute;
import DataStructures.EntityProfile;
import DataStructures.IdDuplicates;
import au.com.bytecode.opencsv.CSVReader;


/***
 * The parser reads a dbpedia data which have a triplets format:
 * <entity url> <ontology_category_i> <value_i> 
 * An original format named Infobox had a property instead of ontology_category,
 * and it was reduced to Ontology format by classifying the properties to ontology categories.
 * 
 * @author Jonathan Svirsky
 */
public class DBPediaParser {
	
	//static Map<String, Set<Integer>> keys 		=null;
	//static HashSet<IdDuplicates> matches  		=null;
	//static ArrayList<EntityProfile> profiles 		=null;
	
	static Map<String, Set<Attribute>> entities	=null;
	static int counter=0;
	
	
	static void readDBPediaNT(String inputName, String outputName, int level, int triplets){
		
		entities=new HashMap<String, Set<Attribute>>();
		
		try {
	
			BufferedReader reader = new BufferedReader(new FileReader(new File(inputName)));
			String currLine=""; 
		    int counter=1;
			while ((currLine = reader.readLine()) != null && counter < triplets){
				String[] parts = currLine.split(" ");
				String key = parts[0];
				String category = parts[1];
				String value = parts[2];
				if (counter==1000000){
					System.out.println(counter+" triplets done!");
				}
				
				key = key.replaceAll("http://dbpedia.org/resource/", "");
				key = key.replaceAll(">", "");
				key = key.replaceAll("<", "");
				
				//parse ontology category:
				category=category.replaceAll("http://dbpedia.org/ontology/", "");
				category=category.replaceAll("http://xmlns.com/foaf/0.1/name", "foaf_name");
				category=category.replaceAll("http://www.w3.org/2003/01/", "w3_");
				category=category.replaceAll(">", "");
				category=category.replaceAll("<", "");
				
				if (level==1)	category = category.split("/")[0];
				else			category=category.replaceAll("\\/", "_");
				
				//clean the value
				value=value.replaceAll("http://dbpedia.org/resource/", "");
				value=value.replaceAll(">", "");
				value=value.replaceAll("<", "");
				value=value.split("http://www.w3.org")[0];
				
				//insert to map
				if (entities.containsKey(key)) {
					entities.get(key).add(new Attribute(category, value));
				}
				else {
					Set<Attribute> set = new HashSet<Attribute>();
					set.add(new Attribute(category, value));
					entities.put(key, set);
				}
			    counter++;
			}
			
			reader.close();
			
		} catch (IOException e) {
			e.printStackTrace();
		}	
	}
	
	
	
	public static void main(String[] args){
		
		System.out.println("Reading first dataset... "+args[0]);
		readDBPediaNT(args[0], null, Integer.parseInt(args[2]), Integer.MAX_VALUE);
		ArrayList<EntityProfile> profiles = new ArrayList<EntityProfile>();
		Map<String, Integer> urlToProfileID1 = new HashMap<String, Integer>();
		
		for (String key : entities.keySet()){
			EntityProfile profile = new EntityProfile(key);
			profile.addAttributes(entities.get(key));
			profiles.add(profile);
			urlToProfileID1.put(key, profiles.size()-1);
		}
		SerializationUtilities.storeSerializedObject(profiles, "dbpedia_0");
		profiles=null;
		System.out.println("size = " + entities.keySet().size());
		
		
		System.out.println("Reading second dataset... "+args[1]);
		readDBPediaNT(args[1], null, Integer.parseInt(args[2]), Integer.MAX_VALUE);
		profiles = new ArrayList<EntityProfile>();
		Map<String, Integer> urlToProfileID2 = new HashMap<String, Integer>();
		for (String key : entities.keySet()){
			EntityProfile profile = new EntityProfile(key);
			profile.addAttributes(entities.get(key));
			profiles.add(profile);
			urlToProfileID2.put(key, profiles.size()-1);
		}
		SerializationUtilities.storeSerializedObject(profiles, "dbpedia_1");
		System.out.println("size = " + entities.keySet().size());
		entities=null;
		
		
		System.out.println("Preparing duplicates file... ");
		Set<String> commonEntities = new HashSet<String>();
		HashSet<IdDuplicates> matches = new HashSet<IdDuplicates>();
		
		commonEntities.addAll(urlToProfileID1.keySet());
		commonEntities.retainAll(urlToProfileID2.keySet());
		
		for (String key: commonEntities){
			IdDuplicates pair = new IdDuplicates(urlToProfileID1.get(key), urlToProfileID2.get(key));
			matches.add(pair);
		}
		SerializationUtilities.storeSerializedObject(matches, "dbpedia_duplicates");
		
		System.out.println("Common keys:" + commonEntities.size());
		
		
	}
	
	
//	/**
//	 * Reads the file and builds a map : <Entity id> to set of attributes.
//	 * It converts the ontology path into the single fall value:
//	 * http://dbpedia.org/ontology/artist/name - > artist_name (level = 2)
//	 * 
//	 * @param fileName
//	 * @param level ontology level
//	 */
//	static void readDBPediaCSV(String inputName, String outputName, int level, int triplets){
//		//profiles = new ArrayList<EntityProfile>();
//		
//		
//		try {
//			//BufferedWriter outputWriter = new BufferedWriter(new FileWriter(new File(outputName)));
//			String[] currLine = null;
//			CSVReader csvReader = new CSVReader(new FileReader(new File(inputName)));
//			//String previousKey = "";
//			int counter = 1;
//			while ((currLine = csvReader.readNext()) != null && counter < triplets) {				
//				
//				String key = currLine[0];
//				String category = currLine[1];
//				String value = currLine[2];
//				
//				//parse ontology category:
//				category=category.replaceAll("http://dbpedia.org/ontology/", "");
//				category=category.replaceAll("http://xmlns.com/foaf/0.1/name", "foaf_name");
//				category=category.replaceAll("http://www.w3.org/2003/01/", "w3_");
//				
//				if (level==1)	category = category.split("/")[0];
//				else			category=category.replaceAll("\\/", "_");
//				
//				category=category.toLowerCase();
//				
//				
//				
//				if (currLine.length < 4){
//					currLine=csvReader.readNext();
//					while (currLine.length < 2){
//						value=value.concat(currLine[0]);
//						currLine=csvReader.readNext();
//					}
//					value=value.concat(currLine[0]);
//				}
//				
//						
//				//insert to map
//				if (entities.containsKey(key)) {
//					entities.get(key).add(new Attribute(category, value));
//				}
//				else {
//					Set<Attribute> set = new HashSet<Attribute>();
//					set.add(new Attribute(category, value));
//					entities.put(key, set);
//				}
//				
//				counter ++;
//			}
//			csvReader.close();
//			
//			
//		} catch (IOException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}	
//	}
}
