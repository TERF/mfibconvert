package mfib.interfaces;

import java.io.Serializable;

public interface SetPairIF extends Serializable {

	public void setPair(int i, int j, double score);
}
