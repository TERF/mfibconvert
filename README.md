# MFIBconvert #

* It is a sub project for MFIB and serves as a first stage for the algorithm:
* Given a set if input datasets and additional parameters, produce a set of required files for MFIBcore. 

In order to run the package please refer to the [wiki][mfibConvertWiki]
[mfibConvertWiki]: https://bitbucket.org/TERF/mfibconvert/wiki/Home


### Contributors: ###

* Jonathan Svirsky
* Sapir Golan
* Batya Kenig